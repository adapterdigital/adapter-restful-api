<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->group(['prefix' => 'v1'], function ($api) {
        $api->post('register', 'App\Api\v1\AuthController@register');
        $api->post('login', 'App\Api\v1\AuthController@login');
        $api->group(['middleware' => ['auth.jwt']], function ($api) {
            $api->post('logout', 'App\Api\v1\AuthController@logout');
            $api->get('user', 'App\Api\v1\AuthController@getAuthUser');

            $api->resource('articles', 'App\Api\v1\ArticleController');
        });
    });
});