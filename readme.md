# Adapter Restful Api Tutorial

## Prerequisite
Be sure that you have Mysql and Composer install in your machine

[Composer Installation](https://getcomposer.org/doc/00-intro.md)

For Mysql You can use XAMPP or WAMP or any web server that you are familiar with

### Step 1: Install Laravel Project

Simply run the command below to install laravel installer in your machine
```
composer global require "laravel/installer"
```

After the installer has been installed, we can create a laravel project with the command below

NOTED: "adapter-api" is the project name. Feel free to change to your desire name

```
composer create-project --prefer-dist laravel/laravel adapter-api
```

### Step 2: Settingup project env(optional)

Start with create new file in adapter-api/bootstrap call it "environment.php" and then copy the code below to it.
```
<?php
    /*
     * |--------------------------------------------------------------------------
     * | Detect The Application Environment
     * |--------------------------------------------------------------------------
     * |
     * | Laravel takes a dead simple approach to your application environments
     * | so you can just specify a machine name for the host that matches a
     * | given environment, then we will automatically detect it for you.
     * |
     * */

    use Dotenv\Dotenv;

    $env = $app->detectEnvironment(function () {
        $environmentPath = __DIR__ . '/../.env';
        if (file_exists($environmentPath)) {
            $setEnv = trim(file_get_contents($environmentPath));
            putenv("APP_ENV=$setEnv");
            if (getenv('APP_ENV')) {
                $environmentFile = __DIR__ . '/../.env.' . getenv('APP_ENV');
                if (file_exists($environmentFile)) {
                    $dotenv = new Dotenv(__DIR__ . '/../', '.env.' . getenv('APP_ENV'));
                    $dotenv->load();
                }
            }
        }
    });
?>
```

Open adapter-api/app/bootstrap/app.php and copy the code below to the section after $app->singleton()
```
/*
  * |--------------------------------------------------------------------------
  * | Load Environment File on Startup
  * |--------------------------------------------------------------------------
  * |
  * | This will determine, which environment will be loaded for our application.
  * |
  * */
require __DIR__.'/environment.php';
```

We need to rename .env to .env.local and create new .env file with the text "local" inside

### Step 3: JWT Installation
Install JWT and Publishing Configuration File and Generate JWT Key
```
composer require tymon/jwt-auth "1.0.*"
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
php artisan jwt:secret
```
The first command will install JWT to you project

The second command will copy the config to your config folder for you to adjust the setting

The last command will generate the jwt keey for you

After JWT has been installed, we need to registering Middleware

Open app/Http/Kernel.php and place the code below to $routeMiddleware array
```
protected $routeMiddleware = [
    //...
    'auth.jwt' => \Tymon\JWTAuth\Http\Middleware\Authenticate::class,
]
```

### Step 4: Install RESTful API package
This step will help you easily controll your routing version and grouping it
```
composer require dingo/api:2.0.0-alpha1
php artisan vendor:publish --provider="Dingo\Api\Provider\LaravelServiceProvider"
```

Set api prefix. 
You can configure this in your .env.local file.
```
API_PREFIX=api
```

### Step 5: Creating API Endpoints
Open adapter-api/routes/api.php and copy the line below to that file
```
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->group(['prefix' => 'v1'], function ($api) {
        $api->post('register', 'App\Api\v1\AuthController@register');
        $api->post('login', 'App\Api\v1\AuthController@login');
        $api->group(['middleware' => ['auth.jwt']], function ($api) {
            $api->post('logout', 'App\Api\v1\AuthController@logout');
            $api->get('user', 'App\Api\v1\AuthController@getAuthUser');
        });
    });
});
```

### Step 6: Update Model
Firset thing first, we want to keep all the models in one area
Simply create new folder called "Models" under the "app" folder
Move User.php to the Models 

JWT requires implementing the Tymon\JWTAuth\Contracts\JWTSubject interface on the User model
Wee have to add two methods getJWTIdentifier and getJWTCustomClaims
To update the user model open app/Models/User.php and edit as bellow

```
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
```

Now add 2 methods
```
/**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
 
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
```

### Step 6: Update Auth Guard
Open adapter-api/app/config/auth.php and update default guard and api guard

```
'defaults' => [
    'guard' => 'api',
    'passwords' => 'users',
],
'guards' => [
    'web' => [
        'driver' => 'session',
        'provider' => 'users',
    ],

    'api' => [
        'driver' => 'jwt',
        'provider' => 'users',
    ],
],
```
Also change user provider module to App\Models\User::class, since we move the user model to another localtion

```
'providers' => [
    'users' => [
        'driver' => 'eloquent',
        'model' => App\Models\User::class, //update here
    ],

    // 'users' => [
    //     'driver' => 'database',
    //     'table' => 'users',
    // ],
],
```

### Step 7: Create JWT Authentication Logic
Create RegisterAuthRequest.php file in app/Http/Requests directory with the command below
```
php artisan make:request RegisterAuthRequest
```

After the file has been created you have to update the rules array like below
```
//...
public function rules()
{
    return [
        'name' => 'required|string',
        'email' => 'required|email|unique:users',
        'password' => 'required|string|min:6|max:10'
    ];
}
```

And also change the authorize to true
```
public function authorize()
{
    return true;
}
```

### Step 8: Create AuthController
Just so you know we will creat the api as a module so we need to create an Api folder under the app folder
The reason being is that we will able to use a shorter controller path in the api routing
To create a controller just run the command below
```
php artisan make:controller App\Api\v1\AuthController
```

Here are the filnal file will look like
```
<?php

namespace App\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterAuthRequest;
use App\Models\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    public $loginAfterSignUp = true;

    public function __construct()
    {
        $this->middleware('auth.jwt', ['except' => ['register','login']]);
    }
 
    public function register(RegisterAuthRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
 
        if ($this->loginAfterSignUp) {
            return $this->login($request);
        }
 
        return response()->json([
            'success' => true,
            'data' => $user
        ], 200);
    }
 
    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $jwt_token = null;
 
        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }
 
        return response()->json([
            'success' => true,
            'token' => $jwt_token,
        ]);
    }
 
    public function logout(Request $request)
    {
        try {
            JWTAuth::invalidate($request->token);
 
            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }
 
    public function getAuthUser(Request $request)
    {
        $user = JWTAuth::authenticate($request->token);
 
        return response()->json(['user' => $user]);
    }
}
```

### Step 9: Migrate database
Before we run the migration command, we need to change a few thing
First, update app/Providers/AppServiceProvider.php
```
use Illuminate\Support\Facades\Schema;
//...
public function boot()
{
    Schema::defaultStringLength(191);
}
```

Then run the migration command 
```
php artisan migrate
```

### Step 10: Testing
```
php artisan serve
```

### Step 11: CORS
To avoid a cors issue, we are going to install another package that take care of the issue for us
```
composer require barryvdh/laravel-cors
php artisan vendor:publish --provider="Barryvdh\Cors\ServiceProvider"
```

To allow CORS for all your routes, add the HandleCors middleware in the $middleware property of app/Http/Kernel.php class:
```
protected $middleware = [
    // ...
    \Barryvdh\Cors\HandleCors::class,
];
```

## RECAP

Create controller
```
php artisan make:controller App\Api\v1\AuthController
```

Create Model
```
php artisan make:model Models/Product -m
```

Create migration only
```
php artisan make:migration create_employyes_table --create=employyes
```

## REALWORD EXAMPLE
```
php artisan make:controller App\Api\v1\ArticleController --resource
php artisan make:model Models/Article -m
php artisan migrate
php artisan make:resource ArticleResource
php artisan make:resource ArticlesResource --collection
```
For more information visit the link below

[https://laravel.com/docs/5.6/eloquent-resources](https://laravel.com/docs/5.6/eloquent-resources)

[http://jsonapi.org/](http://jsonapi.org/)